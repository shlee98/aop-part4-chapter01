package com.example.aop_part4_chapter01.service

import com.example.aop_part4_chapter01.dto.VideoDto
import retrofit2.Call
import retrofit2.http.GET

interface VideoService {

    @GET("/v3/7cdbe80b-b93e-4fd1-9833-92800dccd539")
    fun listVideos(): Call<VideoDto>

}